 # CI/CD Management Console Documentation
<!-- ## Add Project Description -->
## Technology stack
In this project we are using below mentioned software and tools:-
* Database: MongoDB
* Scripts: Python/Shell
* Middleware: NodeJs
* User Interface: ReactJS (Redux framework)
* Authentication: LDAP (Intelâ€™ LDAP for SSO)
* Interface communication: REST API, Web
* socket, LDAP API, AWS API (if applicable)
* Deployment Container: Docker

## Server setup
### Dev Environment setup NodeJS
> Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine.
### Installation
>Linux

#### Step 1 Add Node.js PPA

Assuming that screen (screen) will be used for starting up the micro-serices

Use LTS Release : At the last update of this tutorial, Node.js 12.x is the LTS release available.

```
 sudo apt-get install curl
 curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
 ```

#### Step 2 Install Node.js on Ubuntu

```
 sudo apt-get install nodejs
 ```

#### Step 3 Check Node.js and NPM Version

```
 node -v // v12.13.0
 npm -v // 6.12.0
 ```

### Dev Enviroment setup -Database
>MongoDB is an open-source document database and leading NoSQL database. 
## Installation
>MongoDB on Ubuntu

* Import the public key used by the package management system

``` 
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
```

* Create a list file for MongoDB

```
 echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
```

* Reload local package database.

```
sudo apt-get update 
```

* Install the MongoDB packages.
```
sudo apt-get install -y mongodb-org
```


<!-- * Start MongoDB
```
 sudo service mongod start
```
Begin using MongoDB
```
mongo 
``` -->
### Available Scripts
Assumption:- Copy the code in the repo to 'CICD' Directory on AWS

* Install the Docker-compose packages using "get_dicker.sh" file.
```
sudo chmod +x get_dicker.sh
sudo get_dicker.sh
```

* To up all microservices using docker-compose follow the below commands
> If docker-compose already up then down docker-compose first (Skip 1st command only if not up)
```
docker-compose  down
docker-compose build  
docker-compose build  up -d
```
> To see docker-compose logs, follow the below command
```
docker-compose build  logs -f
```
<!-- >Start basecomponent micro service(It has Hardware, OS, Accelerator APIs and starts on  <https:server-ip:8084>)
```
cd basecomponent 
screen -S basecomponent
npm install
npm start

Press <CTRL> A and D  - to detach from current screen
``` 
>Start User micro service(It has User APIs and starts on  <https:server-ip:8083>)
```
screen -S user
cd ../
cd user 
npm install
npm start

Press <CTRL> A and D  - to detach from current screen
``` 
>Start Recipe micro service(It has Recipe APIs and starts on  <https:server-ip:8090>)
```
screen -S recipe
cd ../
cd recipe 
npm install
npm start

Press <CTRL> A and D  - to detach from current screen
``` 
>Start Ingredient micro service(It has Ingredient APIs and starts on  <https:server-ip:8087>)
```
screen -S ingredient
cd ../
cd ingredient 
npm install
npm start

Press <CTRL> A and D  - to detach from current screen
``` 
>Start API-Gateway micro service(APIs gateway for all above microservices and starts on  <https:server-ip:3002>)
```
screen -S apigw
cd ../
cd api-gateway 
npm install
npm start

Press <CTRL> A and D  - to detach from current screen
``` 
>Start Notification micro service(start on  <https://server-ip:8088>
```
screen -S notif
cd ../
cd notification 
npm install
npm start

Press <CTRL> A and D  - to detach from current screen
``` 
>Start UI micro service(start on  <http:server-ip:3000>, before start goto 'ui/src/_constants/' directory and open config.js file and update(socketUrl :<https://server-ip:8088> and   apiUrl: <https://server-ip:3002>
```
screen -S ui
cd ../
cd ui
npm install
npm start

Press <CTRL> A and D  - to detach th
```  -->
> Note:-  All micro services are now up and running,Open <https:server-ip:3002> for accessing UI, For now you need to allow not-secure session in  secure on browser.To allow the same open these endpoints in the browser and allow Proceed to <ip-address> after clicking advanced. .
> To exit the putty session use ctrl + d

* To Test all microservice (go to in each directory in server and run below command)

### For basecomponents:
```
npm run preinstall
```
```
npm run testOs //For Operating System
```
```
npm run testHw  //For hardware
```
```
npm run testAccelerator   //For Accelerator
```
### For ingredient & recipe
```
npm run preinstall
```
```
npm run test
```
* We can test APIs through POST Man.<br />
* URL [http://localhost:3002] .


### Production

* Perform an optimized production build, generating static HTML and per-route JavaScript code bundles for the best performance.
``` 
npm run build
```

## HTTP Verbs [APIs contracts]

HTTP verbs, or methods, should be used in compliance with their definitions under the [HTTP/1.1]
The action taken on the representation will be contextual to the media type being worked on and its current state. Here's an example of how HTTP verbs map to create, read, update, delete operations in a particular context: <host url://port/"HTTP Methods">


| HTTP METHOD              | POST      |
| ------------------------ | -------------------------- 
| /login       | Input : {userId:<>,password:<> } 
                 Success :{code:200,data:{userId:<>,type:<>}} 
                 Error :{code:<401/404/500/400/503>,mesaage:<>} 

| HTTP METHOD  | POST                       | GET               | PUT (add in route"/id") | DELETE (add in route "/id") |
| ------------ | -------------------------- | ------------------| -------------- | -----------  |
| /basecomponent/hardware    | Create new hardware        | List hardware     | update hardware| Delete H/w   |
| /basecomponent/os          | Create new operating system| List O/S          | update O/S     | Delete O/S   |
| /accelerator | Create new accelerator | List accelerator  | update accelerator | Delete accelerator |
| /ingredient  | Create new ingredient  | List ingredient   | update ingredient | Delete ingredient|
| /recipe      | Create new recipe      | List recipe       | update recipe  | Delete recipe|


Note: For more details of APIs Open Link "Swagger Link"

 Swagger is a set of rules (in other words, a specification) for a format describing REST APIs. As a result, it can be used to share documentation among product managers, testers and developers, but can also be used by various tools to automate API-related processes.

Swagger For hardware, Operating system and Accelerator:
* https://ip-address:8084/api-docs/

![Swagger](Readme-assets/Swagger-os,hardware&accelerator.png?raw=true)


### Swagger For Ingredient:
* https://ip-address:8087/api-docs/

![Ingredient](Readme-assets/Swagger-Ingredient.png?raw=true)

### Swagger For Recipe:
* https://ip-address:8090/api-docs/

![Ingredient](Readme-assets/Swagger-recipe.png?raw=true)


# Database Architecture Design

 We are using MongoDB as the database for this Architecture.

 MongoDB is a document database, which means it stores data in JSON-like documents. We believe this is the most natural way to think about data, and is much more expressive and powerful than the traditional row/column model.
 MongoDB is an open-source document database and leading NoSQL database. Data is stored in collections.

 ## >> Please find the schema for Database below:

![design](Readme-assets/design.png?raw=true)

# There are 6 collections in this Architecture.


# Hardware
*  There is a unique id which is auto-generated by the mongoDB.
*  It contains the name of the Hardware.

# Accelerator
*  There is a unique id which is auto-generated by the mongoDB.
*  It contains the name of the Â Accelerator.

# Ingredient type
*  There is a unique id which is auto-generated by the mongoDB.
*  It contains the name of the IngredientÂ type like (INTEL,Edge,PARTNER    Components) to which group this ingredient belongs.

# Hosts
*  There is a unique id which is auto-generated by the mongoDB.
*  It contains the name of the Hosts.

# Components(Ingredients)
*  There is a unique id which is auto-generated by the mongoDB.
*  It contains the name of the Hosts.
*  ingredientTypeId (TBD)
*  Size in MB
*  Description 
*  More info
*  Git link
*  Download Link
*  Dependency ids that contain dependencies of one ingredient with others.
*  Accelerator ids shows that the give accelerator are compatable with the ingredient.
*  Hosts ids shows that the give Hosts are compatable with the ingredient.
*  Hardware ids shows that the give Hardware are compatable with the ingredient.

# Tags(Recipes)
*  There is a unique id which is auto-generated by the mongoDB.
*  It contains the name of the Recipes.
*  Description
*  Components ids shows that the given Components are included within the Recipes.

# UI Screens

## Login page

![login](Readme-assets/login.png?raw=true)

## Functionalities
*  User can login using their userName & Password.
*  Language can be change while login.
*  User can save login details by clicking on the check box Remember me.
*  Forgot username & password option is also available on the login page.

## Once the user logged in sucessfully.Landing page will displayed as:

## Landing Page

![landing](Readme-assets/Landing-page.png?raw=true)

## Functionalities
*  User can changes the language on this page.
*  User will get Notifications if their is any modifications in the Hardware,Â Accelerator,IngredientÂ type,Operating system,Ingredients & Recipes from any instance of the applications.
*  User can logout by using logout button on the screen.
*  Side Nav is having the collapsable property and have these fields Hardware,Â Accelerator, Operating system ,Ingredients & Recipes.

> #  User can perform Create,Get,Update & Delete for Hardware,Â Accelerator, Operating system ,Ingredients & Recipes.

# Create Operation

![create](Readme-assets/create.gif?raw=true)
*Create operation for hardware*

## Functionalities
*  Create New to create a new hardware and after filling the details we can save the hardware.
*  Multiple language can be used in name field.

## Same can be perform for Accelerator,Ingredient & Operating system.

# Update Operation

![Update](Readme-assets/update.gif?raw=true)
*Update operation for hardware*

## Functionalities
*  By clicking on the edit icon we can modify the details and then click on save to make the changes save. 
*  Once saved the message recieved from the backend is displayed on the snackbar.

## Same can be perform for Accelerator,Ingredient & Operating system.

# Delete Operation

![Delete](Readme-assets/delete.gif?raw=true)
*Delete operation for hardware*

## Functionalities
*  By clicking on the delete icon user can delete the hardware.
*  Popup will come to confirm the delete wheather you want to delete or not . 
*  Once deleted the message recieved from the backend is displayed on the snackbar.

## Same can be perform for Accelerator,Ingredient & Operating system.

# Create Recipe

![create](Readme-assets/create-recipe.gif?raw=true)
*Create operation for recipe*

## Functionalities
*  Create New to create a new recipe and after filling the details we can save the recipe.
*  Multiple languages can be used in Name & Description field.
* User can add ingredients by clicking on the plus icon and choose the required ingredients from the modal table.
* Ingredient Dependency is also implemented here if an ingredient is dependent on others and selected then the dependent ingredient is also selected automatically.


# Update Recipe

![Update](Readme-assets/update-recipe.gif?raw=true)
*Update operation for recipe*

## Functionalities
*  By clicking on the edit icon we can modify the details and then click on save to make the changes save. 
*  Once saved the message received from the backend is displayed on the snack bar.
* Validation is theirs on the ingredient that will not allow a user to delete dependent ingredient.

# Delete Recipe

![Delete](Readme-assets/delete-recipe.gif?raw=true)
*Delete operation for hardware*

## Functionalities
*  By clicking on the delete icon user can delete the recipe.
*  Popup will come to confirm the delete whether you want to delete or not. 
*  Once deleted the message received from the backend is displayed on the snack bar.

# Release Notes:
## CI/CD Management Console 
### v0.0.1
### Dated 11/27/2019 
*  Login page and sign in (Local Authentication No SSO)
*  Landing page , List of ingredients,hardware,os,accelerator.
*  Delete hardware,os,ingredients,accelerator.
*  Add hardware,os,ingredients,accelerator.
*  Update hardware,os,accelerator.
*  User will get Notifications if their is any modifications in the Hardware,Â Accelerator,IngredientÂ type,Operating system,Ingredients & Recipes from any instance of the applications.
*  Swagger and newman for all the above functionalities.


### v0.0.2
### Dated 12/20/2019
* List of Recipe.
*  Delete Recipe.
*  Add Recipe.
*  Update Recipe.
* Manage Ingredient & Recipe.
*  User will get Notifications if their is any modifications in the Recipes from any instance of the applications.
*  Swagger and newman for all the above functionalities.

### v0.0.3
### Dated 01/10/2020
* Integration with Recipe Configurator.   ToDo
*  Deploy CICDMC on new CAAS Portal.      Todo
*  Jenkins integration[Recipe/Ingredients].
*  Logging functionality in backend micro-services.
* Dockerizing Application.
* Integration to Gitlab .
*  Bugs fix.

### v0.0.4
### Dated 01/30/2020
* Docker Deployment in AWS.       
* Support for GitLab Artifact commit.
* Versioning Support for Ingredient(Create gitlab branch for each version).
* Versioning Support for Recipe(Create gitlab branch for each version)..
* Jenkins integration[Recipe/Ingredients].
* Bugs fix.
























